#include<bits/stdc++.h>

using namespace std;

vector<int> pset(1000);

void initSet(int N) {
  pset.resize(N);
  for(int i=0; i<N; ++i) pset[i] = i;
}

int findSet(int i) {
  return (pset[i] == i)? i : pset[i] = findSet(pset[i]);
}

void unionSet(int i, int j) {
  pset[findSet(i)] = findSet(j);
}

bool isSameSet(int i, int j) {
  return findSet(i) == findSet(j);
}

void print() {
  for(int i=0; i<pset.size(); ++i)cout<<Arr[i]<<" ";
  cout<<endl;
}

int main(){
  int cases, i,j;
  cin>>cases;
   while(cases--) {
    string cad, blank;
    getline(cin,blank);
    getline(cin,blank);
    initSet(26);
    while(getline(cin,cad) && !cad.empty()) {
      if(cad.size()==1) continue;
      i = cad[0] - 'A';
      j = cad[1] - 'A';
      unionSet(i,j);
    }
  }
  return 0;
}
