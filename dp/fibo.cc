#include<bits/stdc++.h>

using namespace std;

#define MAX 1000

long long memo[MAX];


long long dp_fibo_memo(long long n) {
  if(memo[n] != -1)
    return memo[n];

  if(n <= 1)
    return memo[n] = n;

  return  memo[n] = dp_fibo_memo(n-1) + dp_fibo_memo(n-2);
}

long long dp_fibo_tab(long long n) {
  long long m[n+1];
  m[0] = 0, m[1] = 1;
  for(int i=2; i<=n; ++i)
    m[i] = m[i-1] + m[i-2];
  return m[n];
}

long long fibo(long long n) {
  if(n<=1)
    return n;
  return fibo(n-1) + fibo(n-2);
}

int main() {
  long long ans,n;
  cin >> n;
  memset(memo,-1, sizeof(long long)*MAX);

  clock_t begin = clock();
  ans = fibo(n);
  clock_t end = clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout <<"recursive sol:"<<ans<<"  time:"<<elapsed_secs<<endl;

  begin = clock();
  ans = dp_fibo_memo(n);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout <<"dp_memo sol:"<<ans<<"  time:"<<elapsed_secs<<endl;

  begin = clock();
  ans = dp_fibo_tab(n);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout <<"dp_tab sol:"<<ans<<"  time:"<<elapsed_secs<<endl;

  return 0;
}
