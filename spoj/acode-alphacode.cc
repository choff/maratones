#include <bits/stdc++.h>
using namespace std;

int main(){
  string s;
  long long dp[5003];
  while(cin>>s && s != "0") {
    if(s.length() == 1) {
      cout<<1<<endl;
      continue;
    };
    
    dp[0] = 1;
    
    for(int i = 0; i < s.length(); i++) {
      int digit = s[i] - '0';
      dp[i+1] = 0;
      if(i==0) {
        dp[i+1] = dp[i];
        continue;
      }
      
      if(digit) 
        dp[i+1] = dp[i];
   
      int twoDigits = digit + (s[i-1] - '0') * 10;
      if( twoDigits <= 26 && twoDigits >= 10) 
        dp[i+1] +=  dp[i-1];

    }
    cout<<dp[s.length()]<<endl;
  }
  return 0;
}
