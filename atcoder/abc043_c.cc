#include<bits/stdc++.h>
using namespace std;

#define fast ios::sync_with_stdio(0); cin.tie(0)

int f_cost(vector<int> nums) {
  int ans = 1<<30;
  
  for(int i = -100; i <= 100; i++) {
    int tmp = 0;
    for(int j = 0; j < nums.size(); j++)
      tmp += (nums[j]-i) * (nums[j]-i);
    if(tmp < ans)
      ans = tmp;
  }
  return ans;
}


int main(){
  fast;
  int cases, num;
  vector<int> nums;
  cin>>cases;
  while(cases--) {
    cin>>num;
    nums.push_back(num);
  }
  cout<<f_cost(nums)<<endl;
  return 0;
}
