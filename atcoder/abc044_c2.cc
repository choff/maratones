#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

typedef long long ll;

ll memo[2501][51][51];

long long dp(int i, long long sum, int elements, int A, vector<int> arr) {
  long ans;
  if(i == (arr.size()-1)){
    if(elements == 0)
       memo[sum][i][elements] = 0;
    else
      memo[sum][i][elements] = (sum/(float)elements == A)? 1 : 0;
    return memo[sum][i][elements];
  }
  
  if (memo[sum][i+1][elements] == -1)
    memo[sum][i+1][elements] = dp(i+1, sum, elements, A, arr);
  
  if (memo[sum+arr[i+1]][i+1][elements+1] == -1)
    memo[sum+arr[i+1]][i+1][elements+1] = dp(i+1, sum+arr[i+1], elements + 1, A, arr);

  ans = memo[sum][i+1][elements] + memo[sum+arr[i+1]][i+1][elements+1];
  return ans;  
}

int main(){
  int N, A;
  memset(memo, -1, sizeof memo); 
  cin>>N>>A;
  vector<int> arr(N);
  for(int i=0; i<N; i++)
    cin>>arr[i];
  cout<<dp(-1,0,0,A,arr)<<endl;
  return 0;
}
