#include <iostream>
#include <vector>

using namespace std;

typedef vector<long long> vll;
typedef vector<vector<long long>> vvll;
typedef vector<vector<vector<long long>>> vvvll;

// state(sum, i, #elements)
vvvll memo(2550, vvll(55, vll(55, -1)));

long long dp(int i, long long sum, int elements, int A, vector<int> arr) {
  long long ans;
  if(i == (arr.size()-1)){
    if(elements == 0)
       memo[sum][i][elements] = 0;
    else
      memo[i][sum][elements] = (sum/(float)elements == A)? 1 : 0;
    //cout<<"i:"<<i<<" sum:"<<sum<<" el:"<<elements<<" ans:"<<memo[i][sum][elements]<<endl;
    return memo[i][sum][elements];
  }
  
  if (memo[sum][i+1][elements] == -1)
    memo[sum][i+1][elements] = dp(i+1, sum, elements, A, arr);
  
  if (memo[sum+arr[i+1]][i+1][elements+1] == -1)
    memo[sum+arr[i+1]][i+1][elements+1] = dp(i+1, sum+arr[i+1], elements + 1, A, arr);

  ans = memo[i+1][sum][elements] + memo[i+1][sum+arr[i+1]][elements+1];
  //cout<<"i:"<<i<<" sum:"<<sum<<" el:"<<elements<<" ans:"<<ans<<endl;
  return ans;  
}

int main(){
  int N, A;
  cin>>N>>A;
  vector<int> arr(N);
  for(int i=0; i<N; i++)
    cin>>arr[i];
  cout<<dp(-1,0,0,A,arr)<<endl;
  return 0;
}
