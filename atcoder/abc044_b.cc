#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

#define fast ios::sync_with_stdio(0); cin.tie(0)

int main(){
  // 97-122
  vector<int> x (26,0);
  string word,ans = "Yes";
  cin>>word;
  for(int i=0; i<word.size(); ++i){
    int idx = (int)word[i] - 97; 
    x[idx] += 1;
  }
  for(int i=0; i<26; ++i){
    if(x[i] % 2 != 0) {
      ans = "No";
      break;
    }
  }
  cout<<ans<<endl;
  return 0;
}
