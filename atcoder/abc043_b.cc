#include <bits/stdc++.h>
using namespace std;

#define fast ios::sync_with_stdio(0); cin.tie(0)

int main(){
  string s, ans="";
  cin>>s;
  for(int i=0; i<s.size(); ++i){
    if(s[i]=='1' | s[i]=='0') 
      ans += s[i];
    else if(s[i]=='B') {
      if(ans.size() > 0){
        ans.pop_back();
      }
    }
  }
  cout << ans << endl;
}
