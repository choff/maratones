#include<bits/stdc++.h>

using namespace std;

int main(){
  set <int> a,b;
  int cases,N;
  cin>>cases;
  while(cases--){
    cin>>N;
    int i,n2;
    int n1[N];
    for(int k=0; k<N; ++k) cin>>n1[k];
    i = 0;
    for(int j=0; j<N; ++j) {
      cin>>n2;
      a.insert(n1[j]);
      b.insert(n2);
      if(a == b) {
        cout<<i+1<<"-"<<j+1<<" ";
        a.clear();
        b.clear();
        i=j+1;
      }
    }
    cout<<endl;
  }
  return 0;
}
