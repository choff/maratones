//////////////////////////////////////////////////////
// problem: root of the problem                     //
// link: https://www.codechef.com/problems/TREEROOT //
//////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
  std::ios::sync_with_stdio(false);
  int cases,N,root,id,sum;
  cin>>cases;
  while(cases--){
    cin>>N;
    root = 0;
    while(N--){
      cin>>id>>sum;
      // id sin padre no es contado en la suma.
      root += id - sum;
    }
    cout<<root<<endl;
  }
  return 0;
}
