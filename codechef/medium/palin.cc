///////////////////////////////////////////////////
// problem: the next palindrome                  //
// link: https://www.codechef.com/problems/PALIN //
///////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

#define endl '\n'

bool isAll9(string cad){
  int len = cad.size();
  int middle = (len % 2)? (len / 2) -1 : len / 2;

  if(len==1)
    return (cad[0] == '9');

  for(int i=0; i<= middle; i++){
    if( cad[i] != '9' || cad[len-1-i] != '9')
      return 0;
  }
  return 1;
}

void nextPalin(string cad){
  int len = cad.size();
  int i = (len / 2) - 1;
  int j = (len % 2)? i+2 : i+1;
  int isPalin = 0;
  int carry = 0;

  while(i>=0 && cad[i] == cad[j])
    i--,j++;

  isPalin = (i<0)? 1 : 0;

  if(isPalin){
    i = (len % 2)? (len / 2) : (len / 2) - 1;
    j = (len % 2)? i : i+1;
    carry = 1;
    for(int k=i,h=j; k>=0; --k,h++){
      int num = cad[k]- '0' + carry;
      carry = num/10;
      cad[k] = (num%10)+'0';
      cad[h] = cad[k];
    }
  }
  else{
    if(cad[i] > cad[j]){
      while(i>=0){
        cad[j] = cad[i];
        i--,j++;
      }
    }
    else{
      i = (len % 2)? (len / 2) : (len / 2) - 1;
      j = (len % 2)? i : i+1;
      carry = 1;
      while(i >= 0){
        int num = cad[i] - '0' + carry;
        carry = num / 10;
        cad[i] = (num % 10) + '0';
        cad[j] = cad[i];
        i--,j++;
      }
    }
  }
  cout<<cad<<endl;
}

int main(){
  std::ios::sync_with_stdio(false);
  string cad;
  int cases;
  cin>>cases;
  while(cases--){
    cin>>cad;
    if(isAll9(cad)){
      cout<<'1';
      for(int i=0; i<cad.size()-1; ++i)cout<<'0';
      cout<<'1';
      cout<<endl;
      continue;
    }
    else if(cad.size() == 1){
      cout<<cad[0]-'0'+1<<endl;
      continue;
    }
    nextPalin(cad);
  }
  return 0;
}
