//////////////////////////////////////////////////////
// problem: Closing the Tweets                      //
// link: https://www.codechef.com/problems/TWTCLOSE //
//////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
  int N,cases,idx;
  string cad;
  while(cin>>N>>cases){
    set<int> S;
    while(cases--){
      cin >> cad;
      if(cad != "CLOSEALL"){
        cin >> idx;
        if(S.count(idx)) S.erase(idx);
        else S.insert(idx);
      }else{
        S.clear();
      }
      cout<<S.size()<<endl;
    }
  }
  return 0;
}
