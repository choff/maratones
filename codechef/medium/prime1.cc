#include<bits/stdc++.h>

using namespace std;


void load_prime(vector<int> &primes){
  vector<int> criba(177,1);
  int j;
  for(int i=2,j=2; i*i<177; i++,j++){
    if(criba[i]){
      for(int k=i+i; k<177; k+=i){
        if(criba[k])criba[k]=0;
      }
      primes.push_back(i);
    }
  }
  for(;j<177; j++){
    if(criba[j])primes.push_back(j);
  }
}

int main(){
  int cases,m,n;
  vector<int> primes;
  load_prime(primes);
  cin>>cases;
  while(cases--){
    cin>>n;
    for(int i=0; i<n; i++){
      cout<<primes[i]<<endl;
    }
  }
  return 0;
}
