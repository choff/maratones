///////////////////////////////////////////////////
// problem: byteland gold coins                  //
// link: https://www.codechef.com/problems/COINS //
///////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

long long f(long long n,unordered_map<int,long long> &coins){
  if(n==0)return 0;
  if(!coins.count(n)){
    coins[n] = max((f(n/2,coins)+f(n/3,coins)+f(n/4,coins)),n);
  }
  return coins[n];
}

int main(){
  long long n;
  unordered_map<int, long long> coins;
  while(cin>>n){
    cout<< max((f(n/2,coins)+f(n/3,coins)+f(n/4,coins)),n)<<endl;
  }
  return 0;
}
