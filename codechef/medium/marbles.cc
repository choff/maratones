////////////////////////////////////////////////////
// problem: marbles                               //
// link:https://www.codechef.com/problems/MARBLES //
////////////////////////////////////////////////////

#include<bits/stdc++.h>

#define ull unsigned long long

using namespace std;

ull solve(int n, int r){
  ull res=1;
  if(r > n/2)
    r = n-r;
  for(int i=0; i<r; ++i){
    res *= n--;
    res /= i+1;
  }
  return res;
}

int main(){
  int cases,marbles,colors,n,r;
  cin>>cases;
  while(cases--){
    cin>>marbles>>colors;
    r = marbles - colors;
    n = colors + r - 1;
    cout<<solve(n,r)<<endl;
  }
  return 0;
}
