 //////////////////////////////////////////////////////
 // problem: Flipping Coins                          //
 // link: https://www.codechef.com/problems/FLIPCOIN //
 //////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int getMid(int s, int e){return s + (e-s)/2;}

//build Segment Tree starting in 1
int buildST(int ST[], int arr[], int ss, int se, int node){

  if(ss == se)
    return ST[node] = arr[ss];

  int mid = getMid(ss,se);

  ST[node] = buildST(ST,arr,ss,mid,2*node) +
    buildST(ST,arr,mid+1,se,2*node+1);

  return ST[node];
}

int queryRange(int ss, int se, int node, int l, int r, int ST[], int lazy[]){
  if(lazy[node]){
    ST[node] = (se - ss + 1) - ST[node]; //flip
    //node has childs
    if(ss != se) {
      lazy[2*node] = 1 - lazy[2*node];
      lazy[2*node+1] = 1 - lazy[2*node+1];
    }
    lazy[node] = 0;
  }

  if(ss >= l && se <= r ) return ST[node];

  if(ss > r || se < l) return 0;

  int mid = getMid(ss,se);

  return queryRange(ss,mid,2*node,l,r,ST,lazy) +
    queryRange(mid+1,se,2*node+1,l,r,ST,lazy);
}


void updateRangeST(int ss, int se, int node, int l, int r, int ST[], int lazy[]){
  //node need be updated
  if(lazy[node]) {
    ST[node] = (se - ss + 1) - ST[node]; //flip
    //node has childs
    if(ss != se) {
      lazy[2*node] = 1 - lazy[2*node];
      lazy[2*node+1] = 1 - lazy[2*node+1];
    }
    lazy[node] = 0;
  }
  //out of range
  if(ss > r || se < l) return;

  //st inside range
  if(ss >= l && se <= r){
    ST[node] = (se - ss + 1) - ST[node]; //flip
    //node has childs
    if(ss != se) {
      lazy[2*node] = 1 - lazy[2*node];
      lazy[2*node+1] = 1 - lazy[2*node+1];
    }
    return;
  }
  int mid = getMid(ss,se);
  updateRangeST(ss,mid,2*node,l,r,ST,lazy);
  updateRangeST(mid+1,se,2*node+1,l,r,ST,lazy);
  ST[node] = ST[2*node] + ST[2*node+1];
}

void load(int Arr[], int len){
  for(int i=0; i<len; ++i)Arr[i]=0;
}

void printST(int ST[], int N){
  for(int i = 0; i < N; ++i){
    cout<<ST[i]<<" ";
  }
  cout<<endl;
}

int main(){
  int N,Q,cmd,A,B;
  while(cin>>N>>Q){
    int arr[N];
    int height = ceil(log2(N));
    int len = 3 * pow(2,height);
    int ST[len];
    int lazy[len];
    load(arr,N);
    buildST(ST,arr,0,N-1,1);
    load(lazy, len);
    while(Q--){
      cin>>cmd>>A>>B;
      if(cmd){
        cout<<queryRange(0,N-1,1,A,B,ST,lazy)<<endl;
      }
      else{
        updateRangeST(0, N-1, 1, A, B, ST, lazy);
      }
    }
  }
  return 0;
}
