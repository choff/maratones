#include<bits/stdc++.h>

using namespace std;

int count(vector<int> &x, int a, int b){
  int ans = 0;
  for(int i=a; i<=b; ++i){
    if(x[i])ans++;
  }
  return ans;
}

void flip(vector<int> &x, int A, int B){
  for(int i=A; i<=B; ++i){
    x[i] = 1 - x[i];
  }
}

int main(){
  int N,Q,A,B,f;
  cin>>N>>Q;
  vector<int> coins(N,0);
  while(Q--){
    cin>>f>>A>>B;
    if(f)
      cout<<count(coins,A,B)<<endl;
    else
      flip(coins,A,B);
    }
  return 0;
}
