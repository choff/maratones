///////////////////////////////////////////////////
// problem: Discrepancies in the Voters List     //
// link:https://www.codechef.com/problems/VOTERS //
///////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
    unordered_map<int, int> M;
    vector<int> ans;
    int x,y,z,tmp;
    cin>>x>>y>>z;
    while (x--) {
        cin>>tmp;
        M[tmp] = (M.count(tmp))? M[tmp]+1 : 1;
    }
    while (y--) {
        cin>>tmp;
        M[tmp] = (M.count(tmp))? M[tmp]+1 : 1;
    }
    while (z--) {
        cin>>tmp;
        M[tmp] = (M.count(tmp))? M[tmp]+1 : 1;
    }
    for (auto& i: M)
        if(i.second >= 2)
            ans.push_back(i.first);
    sort(ans.begin(), ans.end());
    cout<<ans.size()<<endl;
    for(int i=0; i<ans.size(); ++i)
        cout<<ans[i]<<endl;

    return 0;
}
