////////////////////////////////////////////////////
// problem: Permutation Cycles                    //
// link: https://www.codechef.com/problems/PCYCLE //
////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
    int N;
    cin>>N;
    unordered_map<int, vector<int> > ans;

    int idx = 1, flag = 1, pos = 0;

    int per[N+1];
    int visited[N+1];

    memset(visited,0,sizeof(int)*N+1);

    for(int i=1; i<N+1; ++i)
        cin>>per[i];

    if(per[idx] == idx && N==1){
        pos++;
        ans[0].push_back(idx);
        ans[0].push_back(idx);
    }

    while(idx != N) {
        vector<int> local_ans;

        while(visited[idx] == 1) idx++;
        if(idx > N) break;
        visited[idx] = 1;

        int initCycle = idx, j = idx;

        while(per[j] != initCycle) {
            local_ans.push_back(j);
            visited[j] = 1;
            j = per[j];
        }

        local_ans.push_back(j);
        visited[j] = 1;
        local_ans.push_back(initCycle);
        ans[pos++] =  local_ans;
    }
    cout<<pos<<endl;
    for(int k=0; k < pos; ++k){
        for(int l=0; l<ans[k].size(); ++l)
            cout<<ans[k][l]<<" ";
        cout<<endl;
    }

    return 0;
}
