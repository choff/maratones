/////////////////////////////////////////////////////
// problem: paying up                              //
// link: https://www.codechef.com/problems/MARCHA1 //
/////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;


int main(){
  int n,m,cases,sum,flag;
  cin>>cases;
  while(cases--){
    flag=0;
    cin>>n>>m;
    int b[n];
    for(int i=0; i<n; ++i)cin>>b[i];

    for(int i=1; i < (1<<n) && !flag; ++i){
      sum=0;
      for(int j=0; j<n && !flag; ++j){
        if((i & (1<<j)) != 0){
// cout<<j<<" ";
          sum += b[j];
          if(sum == m)flag=1;
        }
      }
    }
    cout<<(flag? "Yes":"No")<<endl;
  }
  return 0;
}
