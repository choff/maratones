/////////////////////////////////////////////////////
// problem: cleaning up                            //
// link: https://www.codechef.com/problems/CLEANUP //
/////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int flag,cases,n,m,done;
  cin>>cases;
  while(cases--){
    cin>>n>>m;
    vector<int> jobs(n+1,0);
    vector<int> cheff,aux;
    for(int k=0; k<m; ++k){
      cin>>done;
      jobs[done] = 1;
    }
//flag: 1-cheff 0-aux
    flag=1;
    for(int k=1; k<=n; ++k){
      if(jobs[k]==0 && flag){
        cheff.push_back(k);
        flag = 1-flag;
      } else if(jobs[k]==0 && !flag){
        aux.push_back(k);
        flag = 1-flag;
      }
    }
    for(int k=0; k<cheff.size(); ++k){
      cout<<cheff[k]<<" ";
    }
    cout<<endl;
    for(int k=0; k<aux.size(); ++k){
      cout<<aux[k]<<" ";
    }
    cout<<endl;
  }
  return 0;
}
