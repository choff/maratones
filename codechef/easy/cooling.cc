////////////////////////////////////////////////////
// problem:cooling pies                           //
// link:https://www.codechef.com/problems/COOLING //
////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int cases, pies;
  cin>>cases;
  while(cases--){
    cin>>pies;
    int ans=0;
    vector<int> wp(pies);
    vector<int> wcr(pies);
    for(int i=0; i<pies; i++)cin>>wp[i];
    for(int i=0; i<pies; i++)cin>>wcr[i];
    sort(wp.begin(),wp.end());
    sort(wcr.begin(),wcr.end());
    int idx = 0; int i=0;
    while(i<pies && idx<pies){
      if(wp[i]<=wcr[idx]){
        i++; idx++;
        ans++;
      }else{
        idx++;
      }
    }
    cout<<ans<<endl;
  }
  return 0;
}
