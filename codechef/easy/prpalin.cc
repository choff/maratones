/////////////////////////////////////////////////////
// problem: prime palindrome                       //
// link: https://www.codechef.com/problems/PRPALIN //
/////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int is_palindrome(int num){
  string s = toStr(num);
  int len = s.size();
  for(int i=0; i<len/2; i++)
    if(s[i]!=s[(len-1)-i])return 0;
  return 1;
}
int is_prime(int n){
  int f=1;
  for(int i=2; i<n && f; i++){
    if(n%i==0)f=0;
  }
  return f;
}

int main(){
  int n;
  long long ans;
  cin>>n;
  ans=n;
  while(1){
    if(is_palindrome(ans) && is_prime(ans))break;
    if(ans%2==0) ans++;
    else ans+=2;
  }
  cout<<ans<<endl;
  return 0;
}
