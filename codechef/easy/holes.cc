///////////////////////////////////////////////////
// problem: Holes in the text                    //
// link: https://www.codechef.com/problems/HOLES //
///////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
  int t,ans;
  string line;
  cin>>t;
  while(t--){
    cin>>line;
    ans=0;
    for(int i=0; i<line.size(); ++i){
      char c=line[i];
      if(c =='A' || c =='D' || c =='O' || c =='P' || c =='R' || c =='Q')ans++;
      if(c == 'B')ans+=2;
    }
    cout<<ans<<endl;
  }
  return 0;
}
