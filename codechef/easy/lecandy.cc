/////////////////////////////////////////////////////
// problem: little elephant and candy              //
// link: https://www.codechef.com/problems/LECANDY //
/////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int cases,N,C,cont,tmp;
  cin>>cases;
  while(cases--){
    cont=0;
    cin>>N>>C;
    for(int i=0; i<N;++i){
      cin>>tmp;
      cont+=tmp;
    }
    cout<<((cont<=C)? "Yes":"No")<<endl;
  }
  return 0;
}
