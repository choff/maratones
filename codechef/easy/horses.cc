////////////////////////////////////////////////////
// problem: racing horses                         //
// link: https://www.codechef.com/problems/HORSES //
////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int T,N,ans;
  cin>>T;
  while(T--){
    cin>>N;
    vector<int> horses(N,0);
    for(int i=0; i<N; ++i)cin>>horses[i];
    sort(horses.begin(),horses.end());
    ans=1<<30;
    for(int j=1; j<N; ++j){
      int diff = abs(horses[j-1]-horses[j]);
      if(diff<ans)ans=diff;
    }
    cout<<ans<<endl;
  }
  return 0;
}
