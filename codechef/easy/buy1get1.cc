//////////////////////////////////////////////////////
// problem: buy 1 get 1                             //
// link: https://www.codechef.com/problems/BUY1GET1 //
//////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int cases,ans;
  string S;
  cin>>cases;
  while(cases--){
    ans=0;
    cin>>S;
    unordered_map<char,int> Map;
    unordered_map<char,int>::iterator it;
    for(int i=0; i<S.size(); ++i){
      if(Map.count(S[i])>0){
        Map[S[i]]++;
      }
      else{
        Map[S[i]] = 1;
      }
    }
    for(it=Map.begin(); it!=Map.end(); it++){
      ans+= ((it->second/2)+(it->second%2));
    }
    cout<<ans<<endl;
  }
  return 0;
}
