///////////////////////////////////////////////////
// problem: ceil and a-b problem                 //
// link:https://www.codechef.com/problems/CIELAB //
///////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int main(){
  int a,b;
  string ans;
  cin>>a>>b;
  ans= toStr(abs(a-b));
  ans[0] = (ans[0]-'0'< 9)? ans[0]+1 : ans[0]-1;
  cout<<ans;
  return 0;
}
