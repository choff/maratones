//////////////////////////////////////////////////////
// problem: count of maximum                        //
// link: https://www.codechef.com/problems/MAXCOUNT //
//////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int main(){
  int cases,n,tmp,cont,digit;
  cin>>cases;
  while(cases--){
    cin>>n;
    digit=1<<30;
    cont=0;
    vector<int> num(10001,0);
    for(int i=0; i<n; ++i){
      cin>>tmp;
      num[tmp]++;
      if(num[tmp]>cont || (num[tmp]==cont && tmp<digit)){
        cont=num[tmp];
        digit=tmp;
      }
    }
    cout<<digit<<" "<<cont<<endl;
  }
  return 0;
}
