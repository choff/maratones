////////////////////////////////////////////////////
// problem: Jewels and Stones                     //
// link: https://www.codechef.com/problems/STONES //
////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
  int cases;
  cin>>cases;
  while(cases--){
    set<char> J;
    string j,s;
    cin>>j>>s;
    int ans = 0;
    for (int i = 0; i < j.size(); ++i)
      J.insert(j[i]);

    for(int i = 0; i < s.size(); ++i)
      if(J.count(s[i]))
        ans++;

    cout<<ans<<endl;
  }
  return 0;
}
