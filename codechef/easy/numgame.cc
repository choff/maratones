/////////////////////////////////////////////////////
// problem: yet anothe number game                 //
// link: https://www.codechef.com/problems/NUMGAME //
/////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int n,cases;
  cin>>cases;
  while(cases--){
    cin>>n;
    cout<<((n%2==0)? "ALICE": "BOB")<<endl;
  }
  return 0;
}
