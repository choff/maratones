////////////////////////////////////////////////////
// problem: Enormous Input Test                   //
// link: https://www.codechef.com/problems/INTEST //
////////////////////////////////////////////////////
#include<bits/stdc++.h>

int main(){
  unsigned long long int n,k,tmp,ans=0;
  scanf("%llu %llu",&n,&k);
  while(n--){
    scanf("%llu",&tmp);
    if(tmp%k==0)ans++;
  }
  printf("%llu\n",ans);
  return 0;
}
