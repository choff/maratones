//////////////////////////////////////////////////////
// problem: sums in a triangle                      //
// link: https://www.codechef.com/problems/SUMTRIAN //
//////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  ios_base::sync_with_stdio(0);
  cin.tie(NULL);
  int t,n,tmp,ans;
  cin>>t;
  while(t--){
    cin>>n;
    ans=0;
    vector<vector<int> > dp(n);
    for(int i=0; i<n; ++i){
      vector<int> r(i+1);
      for(int j=0; j<=i; ++j){
        cin>>r[j];
      }
      dp[i]=r;
      r.clear();
    }
    for(int i=1; i<dp.size(); ++i){
      for(int j=0; j<dp[i].size(); ++j){
        if(j == 0) dp[i][j] += dp[i-1][j];
        else if(j == dp[i].size()-1) dp[i][j] += dp[i-1][j-1];
        else dp[i][j] += max(dp[i-1][j-1],dp[i-1][j]);
        if(i==n-1 && ans<dp[i][j])ans=dp[i][j];
      }
    }
    cout<<(n>1? ans:dp[0][0])<<endl;
  }

  return 0;
}
