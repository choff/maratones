/////////////////////////////////////////////////
// problem: transform the expression           //
// link: https://www.codechef.com/problems/ONP //
/////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

string toRPN(string input){
  string ans;
  stack<char> S;
  char c;
  for(int i=0; i<input.size(); ++i){
    c = input[i];
    if(c == ')'){
      while(!S.empty()){
        c = S.top();
        S.pop();
        if(c=='(')break;
        else ans += c;
      }
      continue;
    }

    if(c == '+' || c == '-' || c == '*' || c == '/' || c == '^'  || c == '('){
      S.push(c);
    }
    else{
      ans += c;
    }
  }
  return ans;
}

int main(){
  int cases;
  cin>>cases;
  string cad;

  while(cases--){
    cin>>cad;
    cout<<toRPN(cad)<<endl;
  }
  return 0;
}
