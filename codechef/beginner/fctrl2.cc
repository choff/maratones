////////////////////////////////////////////////////
// link: https://www.codechef.com/problems/FCTRL2 //
// problem: small factorials                      //
////////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;
vector<int> ans(200);

int main(){
  int T, n, tmp, m, x;
  cin>>T;
  while(T--){
    cin>>n;
    ans[0]=1;
    m=1;
    for(int i=2; i<=n; ++i){
      tmp = 0;
      for(int j=0; j<m; ++j){
        x = ans[j]*i+tmp;
        ans[j] = x%10;
        tmp = x/10;
      }
      while(tmp>0){
        ans[m]=tmp%10;
        tmp/=10;
        m++;
      }
    }
    for(int i=m-1; i>=0; --i)
      cout<<ans[i];
    cout<<endl;
  }
  return 0;
}
