//////////////////////////////////////////////////
// problem: Life, the Universe, and Everything  //
// link: https://www.codechef.com/problems/TEST //
//////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

int main(){
  int n, f=1;
  while(f){
    cin>>n;
    if(n == 42) f=0;
    else cout<<n<<endl;
  }
  return 0;
}
