///////////////////////////////////////////////////
// problem: factorial                            //
// link: https://www.codechef.com/problems/FCTRL //
///////////////////////////////////////////////////
#include<bits/stdc++.h>
using namespace std;
int main(){
  int t,n,ans,tmp;;
  cin>>t;
  while(t--){
    cin>>n;
    ans=0;
    tmp=5;
    while(n/tmp>0){
      ans += n/tmp;
      tmp *= 5;
    }
    cout<<ans<<endl;
  }
}
