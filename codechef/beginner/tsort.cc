///////////////////////////////////////////////////
// problem: turbo sort                           //
// link: https://www.codechef.com/problems/TSORT //
///////////////////////////////////////////////////
#include<bits/stdc++.h>
using namespace std;
int main(){
  int n,tmp;
  scanf("%i",&n);
  vector<int> x(n);
  for(int i=0; i<n; ++i)
    scanf("%i",&x[i]);
  sort(x.begin(),x.end());
  for(int i=0; i<n; ++i)
    printf("%i\n",x[i]);
  return 0;
}
