//////////////////////////////////////////////////////
// problem:packaging cupcakes                       //
// link: https://www.codechef.com/problems/MUFFINS3 //
//////////////////////////////////////////////////////
#include<bits/stdc++.h>
using namespace std;

template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int main(){
  ios_base::sync_with_stdio(0);
  cin.tie(NULL);
  int cases, N;
  cin>>cases;
  for(int i=0; i<cases; ++i){
    cin>>N;
    cout<< N/2+1<<endl;
  }
  return 0;
}
