/////////////////////////////////////////////////////
// problem: ambiguous permutation                  //
// link: https://www.codechef.com/problems/PERMUT2 //
/////////////////////////////////////////////////////
#include<bits/stdc++.h>
using namespace std;

template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int main(){
  ios_base::sync_with_stdio(0);
  cin.tie(NULL);
  int N;
  while(cin>>N,N!=0){
    int a[N], flag=0;
    for(int i=0; i<N; ++i)cin>>a[i];
    for(int i=0; i<N && !flag; ++i)
      if(a[a[i]-1] != i+1) flag=1;
    cout<<(flag? "not ambiguous" : "ambiguous")<<endl;
  }
  return 0;
}
