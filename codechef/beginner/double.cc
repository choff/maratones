//////////////////////////////////////////////////
// problem: double strings                      //
// link: https://www.codechef.com/submit/DOUBLE //
//////////////////////////////////////////////////
#include<bits/stdc++.h>

using namespace std;

int main(){
  int t,n;
  cin>>t;
  while(t--){
    cin>>n;
    cout<<n-n%2<<endl;;
  }
  return 0;
}
