// problem: 725 division

#include<bits/stdc++.h>

using namespace std;

bool isOk(int number1, int number2) {
  int cmp[10] = {0,0,0,0,0,0,0,0,0,0};
  int flag = 1, x = 10000, n1,n2;
  if(number1 > 98765 ) return 0;

  for(int i=0; i<5 && flag; ++i) {
    n1 = number1 / x;
    n2 = number2 / x;
    (cmp[n1])? flag = 0: cmp[n1] = 1;
    (cmp[n2])? flag = 0: cmp[n2] = 1;
    number1 %= x;
    number2 %= x;
    x /= 10;
  }

  return flag;
}

int main() {
  int n,ix = 0;
  while(cin >> n && n) {
    int f = 0;
    if(ix > 0)
      cout<<endl;
    for(int b = 1234; b < 98765; b++) {
      if(isOk( b*n, b)) {
        if(b <= 9999)
          cout<< b*n << " / 0" << b << " = " << n << endl;
        else
          cout<< b*n << " / " << b << " = " << n << endl;
        f = 1;
      }
    }
    if(!f)
      cout<<"There are no solutions for "<<n<<"."<<endl;
    ix++;
  }
  return 0;
}
