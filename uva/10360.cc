//problem: uva 10360 - rat attack

#include<bits/stdc++.h>

#define size 1025

using namespace std;

void fill_m(int x, int y, int p, int d, int &imax, int &jmax, int m[size][size], int &max) {
  int i_min,i_max, j_min,j_max;
  i_min = (x - d < 0 )? 0 : x-d;
  i_max = (x + d > 1024)? 1024 : x + d;
  j_min = (y - d < 0)? 0 : y-d;
  j_max = (y + d > 1024)? 1024 : y + d;

  for(int i = i_min; i <= i_max; ++i) {
    for(int j = j_min; j <= j_max; ++j) {
      m[i][j] += p;
      if(m[i][j] > max) {
        max = m[i][j];
        imax = i , jmax = j;
      }
    }
  }

}
int main() {
  int TC,d,n,x,y,p,imax,jmax,max;
  int m[size][size];
  cin>>TC;
  while(TC--) {
    cin >> d >> n;
    imax = -1, jmax = -1, max = 0;
    memset(m, 0, sizeof m);
    for(int i = 0; i < n; ++i) {
      cin>> x >> y >> p;
      fill_m(x,y,p,d,imax,jmax,m,max);
    }
    cout << imax <<" "<<jmax<<" "<<max<<endl;
  }
  return 0;
}
