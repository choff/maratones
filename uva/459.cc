#include<bits/stdc++.h>

using namespace std;

vector<int> pset(26);

void initSet(int N) {
  pset.resize(N);
  for(int i=0; i<N; ++i) pset[i] = i;
}

int findSet(int i) {
  return (pset[i] == i)? i : pset[i] = findSet(pset[i]);
}

void unionSet(int i, int j) {
  pset[findSet(i)] = findSet(j);
}

bool isSameSet(int i, int j) {
  return findSet(i) == findSet(j);
}

void print(int Arr[], int N) {
  for(int i=0; i<N; ++i)cout<<Arr[i]<<" ";
  cout<<endl;
}

int main(){
  int cases;
  string c;
  getline(cin,c);
  stringstream ss;
  ss << c;
  ss >> cases;
  string blank;
  getline(cin,blank);
   while(cases--) {
     string cad;
     getline(cin,cad);
     int n = cad[0] - 'A' + 1;
     int ans = n;
     initSet(n);
     while(getline(cin,cad) && !cad.empty()) {
       int i = cad[0] - 'A';
       int j = cad[1] - 'A';
       if(findSet(i) != findSet(j)) {
         unionSet(i,j);
         ans--;
       }
     }
     cout<<ans<<endl;
     if(cases > 0) cout << endl;
  }
  return 0;
}
