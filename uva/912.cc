//////////////////////////////////////////////////////////
// problem: uva - 912 live from mars                    //
// link: https://uva.onlinejudge.org/external/9/912.pdf //
//////////////////////////////////////////////////////////

#include<bits/stdc++.h>

using namespace std;

vector<int> pSet(1000000);

void initSet(int N){
  pSet.resize(N);
  for(int i=0; i<N; ++i) pSet[i] = i;
}

int findSet(int i) {
  return ((pSet[i] == i) ? i : pSet[i] = findSet(pSet[i]));
}

void unionSet(int i, int j) {
  pSet[findSet(i)] = findSet(j);
  //printf("pset[%d] = %d, pset[%d] = %d\n",i,pSet[i],j,pSet[j]);
}

bool isSameSet(int i, int j){
  return findSet(i) == findSet(j);
}

int main() {
  int cases = 0;
  int N = 0;
  while(scanf("%d", &N) > 0) {
    initSet(1000000);
    if(cases > 0) printf("\n");
    cases++;

    int A[N] = {0};
    int B[N] = {0};
    bool f = true;
    int in;
    char ccin;
    for(int i=0; i<N; ++i){
      if(scanf("%d",&in) > 0) A[i] = in + 4;
      else {
        scanf("%c",&ccin);
        if(ccin == 'A') A[i] = 0;
        else if(ccin == 'B') A[i] = 1;
        else if(ccin == 'C') A[i] = 2;
        else if(ccin == 'D') A[i] = 3;
      }
    }

    for(int i=0; i<N; ++i){
      if(scanf("%d",&in) > 0) B[i] = in + 4;
      else {
        scanf("%c",&ccin);
        if(ccin == 'A') B[i] = 0;
        else if(ccin == 'B') B[i] = 1;
        else if(ccin == 'C') B[i] = 2;
        else if(ccin == 'D') B[i] = 3;
      }
      //printf("A[%d] = %d, B[%d] = %d\n",i,A[i], i ,B[i]);
      if(!isSameSet(A[i],B[i])){
        //natural to mutant
        if(findSet(A[i]) < 4 && findSet(B[i]) >= 4 ){
          unionSet(B[i], A[i]);
        }
        //mutant to natural
        else if(findSet(A[i]) >= 4 && findSet(B[i]) < 4 )
          unionSet(A[i],B[i]);
        //mutant to mutant
        else if(findSet(A[i]) > 3 && findSet(B[i]) > 3)
          unionSet(A[i], B[i]);
        else
          f=false;
      }
    }
    if(f) {
      char cparent;
      int parent;
      printf("YES\n");
      for(int i=4; i<pSet.size(); ++i) {
        parent = findSet(i);
        if(parent < 4){
          if(parent == 0)
            cparent = 'A';
          if(parent == 1)
            cparent = 'B';
          if(parent == 2)
            cparent = 'C';
          if(parent == 3)
            cparent = 'D';
          printf("%d %c\n",i-4,cparent);
        }
      }
    }
    else
      printf("NO\n");
  }
  return 0;
}
