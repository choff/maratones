// problem: 793 - Network Connections - uva

#include<bits/stdc++.h>

using namespace std;

vector<int> pset(1000);

void initSet(int N) {
  pset.resize(N);
  for(int i=0; i<N; ++i) pset[i] = i;
}

int findSet(int i) {
  return (pset[i] == i)? i : pset[i] = findSet(pset[i]);
}

void unionSet(int i, int j) {
  pset[findSet(i)] = findSet(j);
}

bool isSameSet(int i, int j) {
  return findSet(i) == findSet(j);
}

void print() {
  for(int i=0; i<pset.size(); ++i)cout<<pset[i]<<" ";
  cout<<endl;
}

int main(){
  int cases, i,j,n;
  string line,blank;
  stringstream ss;
  char cmd;
  getline(cin,line);
  ss << line;
  ss >> cases;
  getline(cin,blank);

  while(cases--) {
    int yes=0, no=0;
    getline(cin,line);
    ss.clear();
    ss<<line;
    ss>>n;
    initSet(n+1);
    while(getline(cin,line) && !line.empty()) {
      ss.clear();
      ss << line.c_str();
      ss >> cmd >> i >> j;
      //print();
      if(cmd == 'c')
        unionSet(i,j);
      else
         (findSet(i) == findSet(j))? yes++ : no++;
    }
    cout<<yes<<","<<no<<endl;
    if(cases>0)cout<<endl;
  }
  return 0;
}
