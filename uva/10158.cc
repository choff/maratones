/////////////////////////////////////////////////////////////////////
// Problem: uva - 10158 war                                        //
// Note: I can't do it, this is a nice solution taken from         //
//       http://code.antonio081014.com/2012/09/uva10158warcpp.html //
/////////////////////////////////////////////////////////////////////
#include<bits/stdc++.h>

#define MMAX 10000
using namespace std;

class War {
public:
 int parent;
 int rank; //0, means friends; otherwise, enemy;
};

vector<War> v(MMAX);

void Makeset(int N) {
 for (int i = 0; i <= N; i++) {
  v[i].parent = i;
  v[i].rank = 0;
 }
}

int Find(int x) {
 if (x == v[x].parent)
  return x;
 int temp = v[x].parent;
 v[x].parent = Find(temp);
 v[x].rank = (v[x].rank + v[temp].rank) & 1;
 return v[x].parent;
}

void Union(int t1, int t2, int x, int y, int d) {
 v[t2].parent = t1;
 v[t2].rank = (v[x].rank + v[y].rank + d) & 1;
 Find(x);
}

int Judge(int t1, int t2, int x, int y) {
 if (t1 == t2) {
  if (v[x].rank == v[y].rank)
   return 0; //Friends;
  else
   return 1; //Enemy;
 }
 return -1;
}

int main(int argc, char* argv[]) {

 int N;
 cin >> N;
 Makeset(N);
 int a, b, c;
 while (cin >> a >> b >> c && (a + b + c)) {
  int t1, t2;
  if (a == 1) {
   t1 = Find(b);
   t2 = Find(c);
   if (Judge(t1, t2, b, c) == 1)
    cout << -1 << endl;
   else
    Union(t1, t2, b, c, 0);
  }
  else if (a == 2) {
   t1 = Find(b);
   t2 = Find(c);
   if (Judge(t1, t2, b, c) == 0)
    cout << -1 << endl;
   else
    Union(t1, t2, b, c, 1);
  }
  else if (a == 3) {
   t1 = Find(b);
   t2 = Find(c);
   if (Judge(t1, t2, b, c) == 0)
    cout << 1 << endl;
   else
    cout << 0 << endl;
  }
  else if (a == 4) {
   t1 = Find(b);
   t2 = Find(c);
   if (Judge(t1, t2, b, c) == 1)
    cout << 1 << endl;
   else
    cout << 0 << endl;
  }
 }
 return 0;
}
