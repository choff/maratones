#include<bits/stdc++.h>

using namespace std;

int main(){
    int p = 0, ans = 0, n = 0, c = 5;
    vector<string> x = { "Sheldon", "Leonard", "Penny", "Rajesh", "Howard" };
    cin>>n;
    while (c < n){
        n -= c;
        p++;
        c = 5 * pow(2,p);
    }
    ans = ceil(n / pow(2,p)) - 1;
    cout<<x[ans]<<endl;
    return 0;
}
