/* problem: geneShufle
 * url :http://codeforces.com/gym/101078/attachments/download/4597/20162017-ct-s03e01-codeforces-trainings-season-3-episode-1-en.pdf
 */
#include<bits/stdc++.h>
#define endl '\n'
using namespace std;

template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int cases,N;
  cin>>cases;
  while(cases--){
    cin>>N;
    set <int> a;
    int i=0,n2,last=0;
    int n1[N];
    for(int k=0; k<N; ++k) cin>>n1[k];
    for(int j=0; j<N; ++j) {
      cin>>n2;
      a.insert(n2);
      a.insert(n1[j]);
      if(a.size() == j+1) {
        if(i>0)
          cout<<" ";
        cout<<last+1<<"-"<<j+1;
        i++;
        last = j+1;
      }
    }
    cout<<endl;
  }
  return 0;
}
