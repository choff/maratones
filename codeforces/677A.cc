#include<bits/stdc++.h>

using namespace std;

int main(){
    int n, h, ans = 0, x;
    cin>>n>>h;
    while(n--){
        cin>>x;
        ans += (x>h)? 2 : 1;
    }
    cout<<ans<<endl;
    return 0;
}
