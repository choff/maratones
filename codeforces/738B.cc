#include<bits/stdc++.h>

using namespace std;

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n,m;
  while(cin>>n>>m){
    int a[n][m];
    int ans = 0;

    for(int i=0; i<n; ++i)
      for(int j=0; j<m; ++j)
        cin>>a[i][j];

    for(int i = 0; i < n; ++i){
      int f = 0;
      for(int j = 0; j < m; ++j){
        if(a[i][j] == 1) f = 1;
        else if(f)
          ans++;
      }
      f = 0;
      for(int j = m-1; j >= 0; --j){
        if(a[i][j] == 1) f = 1;
        else if(f)
          ans++;
      }
    }

    for(int col = 0; col < m; ++col){
      int f = 0;
      for(int row = 0; row < n; ++row){
        if(a[row][col] == 1) f = 1;
        else if(f) ans++;
      }
      f = 0;
      for(int row = n-1 ; row >= 0; --row){
        if(a[row][col] == 1) f = 1;
        else if(f) ans++;
      }
    }
    cout << ans<<endl;
  }
  return 0;
}
