#include<bits/stdc++.h>

using namespace std;

int main() {
  int bags, tmp, sum=0,ans=0;
  cin>>bags;
  int c[bags];
  for(int i=0; i< bags; i++) {
    cin>>tmp;
    c[i] = tmp;
    sum += tmp;
  }
  for(int i = 0; i < bags; i++)
    if((sum - c[i])%2 == 0)
      ans++;
  cout<<ans<<endl;
  return 0;
}
