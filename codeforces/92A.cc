#include<bits/stdc++.h>

using namespace std;

int sum(int n) {
  return (n*(n+1))/2;
}

int main() {
  int n,m;
  while(cin>>n>>m) {
    int suma;
    suma = sum(n);
    suma = m % suma;
    for(int i=1; i<=n; ++i){
      if(suma >= i)
        suma -= i;
      else
        break;
    }
    cout<<suma<<endl;
  }
  return 0;
}
