#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,m;
  while(cin>>n>>m) {
    vector<int> set(n);
    int ans = 0;
    for(int i=0; i<n; ++i)
      cin>>set[i];
    sort(set.begin(),set.end());
    for(int i=0; i<m; ++i)
      ans +=(set[i]<0)? set[i] * -1 : 0;
    cout<<ans<<endl;
  }
  return 0;
}
