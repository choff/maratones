#include<bits/stdc++.h>
using namespace std;

int main() {
  int x,y,n;
  cin>>x>>y;
  if((x < 0 && y > 0)||(x > 0 && y< 0))
    n = -x + y;
  else
    n = x + y;

  if (x < 0 && y < 0)
    cout<<n<<" 0  0 "<<n<<endl;
  else if (x < 0 && y > 0)
    cout<<n*-1<<" 0 0 "<<n<<endl;
  else if (x > 0 && y > 0)
    cout<<"0 "<<n<<" "<<n<<" 0"<<endl;
  else
    cout<<"0 "<<n<<" "<<n*-1<<" 0"<<endl;
  return 0;
}
