#include<bits/stdc++.h>
#define endl '\n';

using namespace std;

template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

template <class T>
string toStr(const T &x){stringstream s; s << x; return s.str();}

string noZeros(string x){
  string tmp = "";
  for(int i=0; i < x.length(); ++i)
    if(x[i] != '0')
      tmp+=x[i];
  return tmp;
}

int main() {
  //ios_base::sync_with_stdio(false);
  //cin.tie(NULL);
  int a,b,a1,b1,f;
  long long c,d;
  while(cin>>a>>b) {
    c = a + b;
    f = 0;
    a1 = toInt(noZeros(toStr(a)));
    b1 = toInt(noZeros(toStr(b)));
    d =  a1 + b1;
    if(toInt(noZeros(toStr(c))) == d)
      f=1;
    cout<<((f)? "YES" : "NO")<<endl;
  }
  return 0;
}
