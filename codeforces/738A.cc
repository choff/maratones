#include<bits/stdc++.h>
#include <regex>

//accepted : http://codeforces.com/contest/738/problem/A
using namespace std;

int main() {
    std::regex vowel_re("o(go)+");
    int n;
    string s,ans;
    while(cin>>n>>s){
        regex_replace(std::back_inserter(ans),s.begin(), s.end(), vowel_re, "***");
        cout<<ans<<endl;
    }
    return 0;
}
