#include<bits/stdc++.h>

using namespace std;

int main() {
  int files,flag=0;
  string row;
  vector<string> bus;
  cin>>files;
  for(int i=0; i<files; i++) {
    cin>>row;
    if(row[0] == 'O' && row[1] == 'O' && !flag) {
      flag = 1;
       row[0] = '+', row[1] = '+';
    }
    else if(row[3] == 'O' && row[4] == 'O' && !flag) {
      flag = 1;
      row[3] = '+', row[4] = '+';
    }
    bus.push_back(row);
  }
  if(flag){
    cout<<"YES"<<endl;
    for(int i = 0; i < files; i++)
      cout<< bus[i] << endl;
  }
  else
    cout<<"NO"<<endl;
  return 0;
}
