#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,ans,tmp;
  while(cin>>n) {
    tmp =  n - 10;
    if(tmp <= 0 || tmp > 11)
      ans = 0;
    else if((tmp>0 && tmp < 10) || tmp == 11)
      ans = 4;
    else if(tmp == 10)
      ans = 15;
    cout<<ans<<endl;
  }
  return 0;
}
