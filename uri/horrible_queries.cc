#include<bits/stdc++.h>

using namespace std;

#define ll long long

ll getMid(ll s, ll e){return s + (e-s)/2;}
void load(ll arr[], ll N){ for(ll i=0; i<N; i++)arr[i] = 0;}
void printArr(ll arr[], ll N){
  for(ll i=0; i<N; i++)
    cout<<arr[i]<<" ";
  cout<<endl;
}

ll buildST(ll ss, ll se, ll node, ll ST[], ll arr[]){
  if(ss == se)
    return ST[node] = arr[ss];
  ll mid = getMid(ss,se);

  return ST[node] = buildST(ss, mid, 2*node, ST,arr) +
    buildST(mid+1,se,2*node+1,ST,arr);
}

void updateRange(ll ss, ll se, ll node, ll l, ll r, ll v, ll ST[], ll arr[], ll Lazy[]){
  //hay actualizaciones pendientes
  if(Lazy[node]){
    ST[node] += (se-ss+1) * Lazy[node];
    //nodo tiene hijos
    if(ss != se){
      Lazy[2*node] += Lazy[node];
      Lazy[2*node+1] += Lazy[node];
    }
    Lazy[node] = 0;
  }
  //fuera de rango
  if(ss > r || se < l) return;

  if(ss >= l && se <= r){
    ST[node] += (se - ss + 1) * v;
    if(ss != se){
      Lazy[2*node] += v;
      Lazy[2*node+1] += v;
    }
    return;
  }

  ll mid = getMid(ss,se);
  updateRange(ss, mid, 2*node, l, r, v, ST, arr, Lazy);
  updateRange(mid+1, se, 2*node+1, l, r, v, ST, arr, Lazy);
  ST[node] = ST[2*node] + ST[2*node+1];
}

ll queryRange(ll ss, ll se, ll node, ll l, ll r, ll ST[], ll Lazy[]){
  //hay operaciones pendientes
  if(Lazy[node]){
    ST[node] += (se-ss+1) * Lazy[node];
    if(ss != se){
      Lazy[2*node] += Lazy[node];
      Lazy[2*node+1] += Lazy[node];
    }
    Lazy[node] = 0;
  }
  //fuera de rango
  if(ss > r || se < l) return 0;

  //dentro del rango
  if(ss >= l && se <= r) return ST[node];

  ll mid = getMid(ss,se);
  return queryRange(ss, mid, 2*node, l, r, ST, Lazy) +
    queryRange(mid+1, se, 2*node+1, l, r, ST, Lazy);
}

int main(){
  ll T,N,C,p,q,v,cmd;
  cin>>T;
  while(T--){
    cin>>N>>C;
    ll height = ceil(log2(N));
    ll len = 2 * pow(2,height);

    ll arr[N];
    ll ST[len];
    ll Lazy[len];

    load(arr,N);
    load(Lazy,len);
    buildST(0, N-1, 1, ST, arr);

    while(C--){
      cin>>cmd;
      if(cmd){
        cin>>p>>q;
        cout<<queryRange(0, N-1, 1, p-1, q-1, ST, Lazy)<<endl;
      }
      else{
        cin>>p>>q>>v;
        updateRange(0, N-1, 1, p-1, q-1, v, ST, arr, Lazy);
      }
    }
  }
  return 0;
}
