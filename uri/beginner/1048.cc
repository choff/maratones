#include<bits/stdc++.h>

using namespace std;

bool comp(double i, double j){return (i>j);}
int main(){
  double n,mas;
  int porc;
  cin>>n;
  if(n>=0 && n<=400.00){
    mas = n*0.15;
    porc = 15;
  }
  if(n>400.00 && n<=800.00){
    mas = n*0.12;
    porc = 12;
  }
  if(n>800 && n<=1200.00){
    mas = n*0.10;
    porc = 10;
  }
  if(n > 1200 && n <= 2000.00){
    mas = n*0.07;
    porc = 7;
  }
  if(n>2000 ){
    mas = n*0.04;
    porc = 4;
  }
  cout<<fixed<<setprecision(2)<<"Novo salario: "<<n+mas<<endl;
  cout<<fixed<<setprecision(2)<<"Reajuste ganho: "<<mas<<endl;
  cout<<"Em percentual: "<<porc<<" %"<<endl;
  return 0;
}
