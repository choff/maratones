#include<bits/stdc++.h>

#define MAX 100

using namespace std;

int main() {
  double n, arr[MAX];
  cin>>n;
  arr[0] = n;
  for(int i=1; i<MAX; ++i){
    arr[i] = arr[i-1]/2;
  }
  for(int i=0; i<MAX; ++i)
    cout<<fixed<<setprecision(4)<<"N["<<i<<"] = "<<arr[i]<<endl;
  return 0;
}
