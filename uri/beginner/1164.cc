#include<bits/stdc++.h>

using namespace std;

bool isPerfect(int n){
  int sum = 0;
  for(int i=1; i<n && sum<=n; ++i){
    if(n % i == 0)
      sum+=i;
  }
  return (sum != n)? 0 : 1;
}

int main() {
  int n,tmp;
  cin>>n;
  while(n--) {
    cin>>tmp;
    if(isPerfect(tmp))
      cout<<tmp<<" eh perfeito"<<endl;
    else
      cout<<tmp<<" nao eh perfeito"<<endl;
  }
  return 0;
}
