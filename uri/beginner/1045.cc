#include<bits/stdc++.h>

using namespace std;

bool comp(double i, double j){return (i>j);}
int main(){
  double arr[3];
  double A,B,C;
  for(int i=0; i<3; i++) cin>>arr[i];
  sort(arr,arr+3,comp);
  A = arr[0], B = arr[1], C = arr[2];

  if(A >= B + C)
    cout<<"NAO FORMA TRIANGULO"<<endl;
  else{
    if(A*A == B*B + C*C)
      cout<<"TRIANGULO RETANGULO"<<endl;
    if(A*A > B*B + C*C)
      cout<<"TRIANGULO OBTUSANGULO"<<endl;
    if(A*A < B*B + C*C)
      cout<<"TRIANGULO ACUTANGULO"<<endl;
    if(A == B && A == C)
      cout<<"TRIANGULO EQUILATERO"<<endl;
    if( (A == B && A!=C) || (A == C && A!=B) || (B == C && B!=A))
      cout<<"TRIANGULO ISOSCELES"<<endl;
  }
  return 0;
}
