#include<bits/stdc++.h>

using namespace std;

int main() {
  int n;
  while(cin>>n) {
    int f=-1, prev=0,tmp,ans=1;
    cin>>prev;
    for(int i=1; i<n; ++i) {
      cin>>tmp;
      if(tmp == prev) ans = 0;
      if(f == -1) f = (tmp < prev )? 1 : 0;
      else if ((tmp < prev && !f) || (tmp > prev && f)) ans=0;
      f = 1 - f;
      prev = tmp;
    }
    cout<<ans<<endl;
  }
  return 0;
}
