#include<bits/stdc++.h>

using namespace std;

int main() {
  int valid=0,f=1;
  double tmp,ans=0;
  while(f){
    valid = 0;
    ans = 0;
    while(valid<2) {
      cin>>tmp;
      if(tmp<0 || tmp>10)
        cout<<"nota invalida"<<endl;
      else {
        ans+=tmp;
        valid++;
      }
    }
    cout<<fixed<<setprecision(2)<<"media = "<<ans/(double)2<<endl;
    while(1){
      cin>>tmp;
      cout<<"novo calculo (1-sim 2-nao)"<<endl;
      if(tmp == 1){
        break;
      }
      else if(tmp==2){
        f=0;
        break;
      }
    }
  }
  return 0;
}
