#include<bits/stdc++.h>

using namespace std;

vector<long long> fibo(46,0);
void loadFibo(){
  fibo[0] = 0;
  fibo[1] = 1;
  for(int i=2; i < fibo.size(); ++i)
    fibo[i] = fibo[i-1] + fibo[i-2];
}

int main() {
  int n;
  loadFibo();
  while(cin>>n){
    for(int i=0; i<n-1; ++i)
      cout<<fibo[i]<<" ";
    cout<<fibo[n-1]<<endl;
  }
  return 0;
}
