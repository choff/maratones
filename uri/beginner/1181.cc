#include<bits/stdc++.h>

#define MAX 100

using namespace std;

int main() {
  double M[12][12];
  int line;
  char op;
  double ans=0;
  cin>>line>>op;
  for(int i=0; i<12; ++i)
    for(int j=0; j<12; ++j)
      cin>>M[i][j];
  for(int k=0; k<12; ++k)
    ans += M[line][k];
  if(op == 'M')
    ans = ans/12;
  cout<<fixed<<setprecision(1)<<ans<<endl;
  return 0;
}
