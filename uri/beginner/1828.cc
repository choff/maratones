#include<bits/stdc++.h>

using namespace std;

int validate(string a, string b) {
  //pedra, papel, tesoura, lagarto e Spock
  if(a == "tesoura" && b =="papel" ||
     a == "papel" && b == "pedra" ||
     a == "pedra" && b == "lagarto" ||
     a == "lagarto" && b == "Spock" ||
     a == "Spock" && b == "tesoura" ||
     a == "tesoura" && b == "lagarto" ||
     a == "lagarto" && b == "papel" ||
     a == "papel" && b == "Spock" ||
     a == "Spock" && b == "pedra" ||
     a == "pedra" && b == "tesoura")
    return 1;
  else if(a == b)
    return 0;
  return -1;
}
int main(){
  int cases,cont=1;
  string a,b;
  cin>>cases;
  while(cases--) {
    cin>>a>>b;
    cout<<"Caso #"<<cont++<<": ";
    if(validate(a,b) == 0)
      cout<<"De novo!"<<endl;
    else if(validate(a,b) == 1)
      cout<<"Bazinga!"<<endl;
    else
      cout<<"Raj trapaceou!"<<endl;
  }
  return 0;
}
