#include<bits/stdc++.h>
#define MAX 10000001
using namespace std;

vector<bool> primos(MAX,0);

void loadPrimos(){
  vector<bool> criba(MAX,1);
  for(int i=2; i<MAX; i++) {
    if(criba[i]){
      primos[i] = 1;
      for(int j=2; j*i<MAX; ++j)
        criba[j*i] = 0;
    }
  }
}
int main() {
  int n,tmp;
  cin>>n;
  loadPrimos();
  while(n--) {
    cin>>tmp;
    if(primos[tmp])
      cout<<tmp<<" eh primo"<<endl;
    else
      cout<<tmp<<" nao eh primo"<<endl;
  }
  return 0;
}
