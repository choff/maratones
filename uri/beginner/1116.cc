#include<bits/stdc++.h>

using namespace std;

int main() {
  int cases,x,y;
  cin>>cases;
  while(cases--) {
    cin>>x>>y;
    if(y == 0)
      cout<<"divisao impossivel"<<endl;
    else
      cout<<fixed<<setprecision(1)<<x/(double)y<<endl;
  }
  return 0;
}
