#include<bits/stdc++.h>

using namespace std;

int main(){
  double pi = 3.14159;
  double r;
  cin>>r;
  cout<<fixed<<setprecision(4)<<"A="<<pi*r*r<<endl;
  return 0;
}
