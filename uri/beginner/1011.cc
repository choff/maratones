#include<bits/stdc++.h>

using namespace std;

int main(){
  double pi = 3.14159;
  double r,ans;
  cin>>r;
  ans = (4.0/3.0) * pi * r * r * r;
  cout<<fixed<<setprecision(3)<<"VOLUME = "<<ans<<endl;

  return 0;
}
