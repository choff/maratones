#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,tmp,f;
  while(cin>>n && n){
    int arr[n][n];
    f = (n%2==0)? 0:1;
    for(int i=0; i< n/2 + f; i++) {
      tmp=0;
      for(int j=0; j< n/2 + f; ++j) {
        arr[i][j] = tmp+1;
        arr[i][n-1-j] = tmp+1;
        arr[n-1-i][j] = tmp+1;
        arr[n-1-i][n-1-j] = tmp+1;
        if(tmp!=i) tmp++;
      }
    }
    for(int i=0; i<n; ++i){
      for(int j=0; j<n; ++j){
        printf("%3d",arr[i][j]);
        if(j != n-1) printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
  return 0;
}
