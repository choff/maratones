#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,C=0,R=0,S=0,tmp;
  double ans;
  char c;
  cin>>n;
  while(n--) {
    cin>>tmp>>c;
    if(c == 'C')
      C += tmp;
    else if(c == 'R')
      R += tmp;
    else if(c == 'S')
      S += tmp;
  }
  tmp = C+R+S;
  cout<<"Total: "<<tmp<<" cobaias"<<endl;

  cout<<"Total de coelhos: "<<C<<endl;
  cout<<"Total de ratos: "<<R<<endl;
  cout<<"Total de sapos: "<<S<<endl;
  cout<<"Percentual de coelhos: "<<fixed<<setprecision(2)<<(C/(double)tmp)*100<<" %"<<endl;
  cout<<"Percentual de ratos: "<<fixed<<setprecision(2)<<(R/(double)tmp)*100<<" %"<<endl;
  cout<<"Percentual de sapos: "<<fixed<<setprecision(2)<<(S/(double)tmp)*100<<" %"<<endl;
  return 0;
}
