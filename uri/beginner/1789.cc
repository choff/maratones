#include<bits/stdc++.h>

using namespace std;

int main(){
  int n, ans, tmp;
  while(cin>>n) {
    ans=0;
    for(int i=0; i<n; ++i) {
      cin>>tmp;
      if(tmp>ans)
        ans = tmp;
    }
    if(ans<10) ans = 1;
    else if(ans >= 10 && ans < 20) ans = 2;
    else if(ans>=20) ans = 3;
    cout<<ans<<endl;
  }

  return 0;
}
