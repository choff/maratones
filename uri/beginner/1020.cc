#include<bits/stdc++.h>

using namespace std;

int main(){
    int n, years, month, days;
    cin>>n;
    years = n / 365;
    month = (n % 365) / 30;
    days = (n % 365) % 30;
    cout<<years<<" ano(s)"<<endl;
    cout<<month<<" mes(es)"<<endl;
    cout<<days<<" dia(s)"<<endl;
    return 0;
}
