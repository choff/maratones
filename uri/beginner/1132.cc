#include<bits/stdc++.h>

using namespace std;

int main() {
  int x,y,ans=0,tmp;
  while(cin>>x>>y) {
    tmp=x;
    x = min(x,y);
    y = max(tmp,y);
    for(int i=x; i<=y; ++i) {
      if(i%13!=0)ans += i;
    }
    cout<<ans<<endl;
  }
  return 0;
}
