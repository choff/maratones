#include<bits/stdc++.h>

#define MAX 61
using namespace std;
vector<unsigned long long> fibo(MAX);

void loadFibo(){
  fibo[0] = 0;
  fibo[1] = 1;
  for(int i=2; i<MAX; ++i)
    fibo[i] = fibo[i-2] + fibo[i-1];
}
int main() {
  int cases,tmp;
  loadFibo();
  cin>>cases;
  while(cases--){
    cin>>tmp;
    cout<<"Fib("<<tmp<<") = "<<fibo[tmp]<<endl;
  }
  return 0;
}
