#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,x;
  while(cin>>n) {
    vector<vector<int> > arr(n, vector<int>(n,0));
    x = 0;
    for(int y=0; y<n/3; ++y,x++) {
      arr[y][x] = 2;
      arr[y][n-1-x] = 3;
      arr[n-1-y][x] = 3;
      arr[n-1-y][n-1-x] = 2;
    }

    for(int y=n/3; y <(n - n/3); y++) {
      for(int x=n/3; x < (n - n/3); x++)
        arr[y][x] = 1;
    }

    arr[n/2][n/2] = 4;

    for(int a=0; a<n; a++){
      for(int b=0; b<n; b++)
        cout<<arr[a][b];
      cout<<endl;
    }
    cout<<endl;
  }
  return 0;
}
