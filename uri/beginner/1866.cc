#include<bits/stdc++.h>

using namespace std;

int main(){
  int cases,tmp;
  cin>>cases;
  while(cases--){
    cin>>tmp;
    cout<<((tmp%2 == 0)? 0 : 1)<<endl;
  }
  return 0;
}
