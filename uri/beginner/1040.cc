#include<bits/stdc++.h>

using namespace std;

int main(){
  double a,b,c,d,media,exam;
  cin>>a>>b>>c>>d;
  media = (a*2 + b*3 + c*4 + d*1) / 10.0;
  cout<<fixed<<setprecision(1)<<"Media: "<<media<<endl;
  if(media >= 7.0)
    cout<<"Aluno aprovado."<<endl;
  else if(media < 5.0)
    cout<<"Aluno reprovado."<<endl;
  else if(media >= 5.0 && media <= 6.9){
    cout<<fixed<<setprecision(1);
    cout<<"Aluno em exame."<<endl;
    cin>>exam;
    cout<<fixed<<setprecision(1)<<"Nota do exame: "<<exam<<endl;
    media = (media + exam) / 2.0;
    if(media >= 5.0)
      cout<<"Aluno aprovado."<<endl;
    else
      cout<<"Aluno reprovado."<<endl;
    cout<<fixed<<setprecision(1)<<"Media final: "<<media<<endl;

  }
  return 0;
}
