#include<bits/stdc++.h>

using namespace std;

int main() {
  int cases,n,y,ans;
  cin>>cases;
  while(cases--) {
    ans=0;
    cin>>n>>y;
    if(n%2==0)
      n++;
    for(int i=0; i<y; ++i){
      ans+= n+i*2;
    }
    cout<<ans<<endl;
  }
  return 0;
}
