#include<bits/stdc++.h>

using namespace std;

int numDigits(int i){
  int ans=0;
  while(i>0) {
    ans++;
    i = i/10;
  }
  return ans;
}

int main() {
  int n,tmp,T;
  while(cin>>n && n){
    int arr[n][n];
    int max=0;
    for(int i=0; i< n; i++) {
      tmp=i;
      for(int j=0; j< n; ++j) {
        arr[i][j] = pow(2,tmp);
        if(arr[i][j] > max) max = arr[i][j];
        tmp++;
      }
    }
    T=numDigits(max);

    for(int i=0; i<n; ++i){
      for(int j=0; j<n; ++j){
        int numd = numDigits(arr[i][j]);
        for(int i=0; i<T-numd; ++i)printf(" ");
        printf("%d",arr[i][j]);
        if(j != n-1) printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
  return 0;
}
