#include<bits/stdc++.h>

using namespace std;

int main(){
  int n;
  unordered_map<int,string> M;
  M[61] = "Brasilia";
  M[71] = "Salvador";
  M[11] = "Sao Paulo";
  M[21] = "Rio de Janeiro";
  M[32] = "Juiz de Fora";
  M[19] = "Campinas";
  M[27] = "Vitoria";
  M[31] = "Belo Horizonte";

  cin>>n;

  if(M.count(n))
    cout<<M[n]<<endl;
  else
    cout<<"DDD nao cadastrado"<<endl;

  return 0;
}
