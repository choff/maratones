#include<bits/stdc++.h>

using namespace std;

vector<long long> fac(14,0);
void loadFac(){
  fac[0] = 0;
  fac[1] = 1;
  for(int i=2; i < fac.size(); ++i)
    fac[i] = fac[i-1] * i;
}

int main() {
  int n;
  loadFac();
  while(cin>>n){
    cout<<fac[n]<<endl;
  }
  return 0;
}
