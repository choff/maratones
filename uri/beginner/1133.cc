#include<bits/stdc++.h>

using namespace std;

int main() {
  int x,y,tmp;
  while(cin>>x>>y) {
    tmp=x;
    x = min(x,y);
    y = max(tmp,y);
    for(int i=x+1; i<y; ++i)
      if(i%5==2 || i%5==3)
        cout<<i<<endl;
  }
  return 0;
}
