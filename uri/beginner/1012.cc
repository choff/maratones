#include<bits/stdc++.h>

using namespace std;

int main(){
  double pi = 3.14159;
  double A,B,C, A_t, A_c, A_tra, A_s, A_r;
  cin>>A>>B>>C;
  A_t = 0.5 * A * C;
  A_c = pi * C * C;
  A_tra = (0.5)*(A+B) * C;
  A_s = B*B;
  A_r = A*B;
  cout<<fixed<<setprecision(3)<<"TRIANGULO: "<<A_t<<endl;
  cout<<fixed<<setprecision(3)<<"CIRCULO: "<<A_c<<endl;
  cout<<fixed<<setprecision(3)<<"TRAPEZIO: "<<A_tra<<endl;
  cout<<fixed<<setprecision(3)<<"QUADRADO: "<<A_s<<endl;
  cout<<fixed<<setprecision(3)<<"RETANGULO: "<<A_r<<endl;
  return 0;
}
