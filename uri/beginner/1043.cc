#include<bits/stdc++.h>

using namespace std;

int main(){
  double A,B,C;
  int flag=1;
  cin>>A>>B>>C;

  if( !(A < (B+C) && A > (B-C)) ) flag=0;
  if( !(B < (A+C) && B > (A-C)) ) flag=0;
  if( !(C < (B+A) && C > (B-A)) ) flag=0;
  if(flag)
    cout<<fixed<<setprecision(1)<<"Perimetro = "<<A+B+C<<endl;
  else
    cout<<fixed<<setprecision(1)<<"Area = "<<((A+B)*C)/2.0<<endl;

  return 0;
}
