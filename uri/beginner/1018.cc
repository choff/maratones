#include<bits/stdc++.h>

template <class charT, charT sep>
class punct_facet: public std::numpunct<charT> {
protected:
  charT do_decimal_point() const { return sep; }
};

using namespace std;

double bn[7] = {100,50,20,10,5,2,1};
int ans[7] = {0,0,0,0,0,0,0};

int main() {
  std::cout.imbue(std::locale(std::cout.getloc(), new punct_facet<char, ','>));
  int n,i=0;
  cin>>n;
  cout<<n<<endl;
  while(n>0 && i<7) {
    if(n >= bn[i]) {
      ans[i] = int(n/bn[i]);
      n = n % (int)bn[i];
    }
    else i++;
  }
  for(int j=0; j<7; ++j){
    cout<<fixed<<setprecision(2)<<ans[j]<<" nota(s) de R$ "<<bn[j]<<endl;
  }
  return 0;
}
