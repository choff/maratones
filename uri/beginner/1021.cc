#include<bits/stdc++.h>

using namespace std;

int main(){
    int idn=0, idc=0;
    double n;
    cin>>n;
    n += 1e-9;
    double notes[6] = {100, 50, 20, 10, 5, 2};
    double coins[6] = {1, 0.5, 0.25, 0.10, 0.05, 0.01};

    int b[6] = {0,0,0,0,0,0};
    int c[6] = {0,0,0,0,0,0};

    while(n>=2 && idn<6) {
        if(n >= notes[idn]) {
            int tmp = n / notes[idn];
            b[idn] = tmp;
            n = n - (tmp * notes[idn]);
            idn++;
        }
        else idn++;
    }

    while(n > 0 && idc<6) {
        if(n >= coins[idc]) {
            int tmp = n / coins[idc];
            c[idc] = tmp;
            n = n - (tmp*coins[idc]);
            idc++;
        }
        else idc++;
    }
    cout<<"NOTAS:"<<endl;
    for(int i=0; i<6; ++i){
      cout<<fixed<<setprecision(2)<<b[i]<<" nota(s) de R$ "<<notes[i]<<endl;
    }
    cout<<"MOEDAS:"<<endl;
    for(int i=0; i<6; ++i){
      cout<<fixed<<setprecision(2)<<c[i]<<" moeda(s) de R$ "<<coins[i]<<endl;
    }
    return 0;
}
