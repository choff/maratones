#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,tmp;
  cin>>n;
  while(n--) {
    cin>>tmp;
    if(tmp > 0)
      cout<<((tmp%2==0)? "EVEN POSITIVE" : "ODD POSITIVE")<<endl;
    else if(tmp < 0)
      cout<<(((tmp*-1)%2==0)? "EVEN NEGATIVE" : "ODD NEGATIVE")<<endl;
    else if(tmp == 0)
      cout<<"NULL"<<endl;
  }
  return 0;
}
