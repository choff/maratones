#include<bits/stdc++.h>

#define MAX 20
using namespace std;

int main() {
  double x[MAX],tmp;
  for(int i=0; i<MAX; ++i) {
    cin>>x[i];
  }
  for(int i=0; i< MAX/2; ++i) {
    tmp = x[MAX-i-1];
    x[MAX-1-i] = x[i];
    x[i] = tmp;
  }
  for(int i=0; i<MAX; ++i)
    cout<<"N["<<i<<"] = "<<x[i]<<endl;
  return 0;
}
