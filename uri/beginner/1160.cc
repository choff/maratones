#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,ans=0;
  double pa,pb,g1,g2;
  cin>>n;
  while(n--) {
    ans = 0;
    cin>>pa>>pb>>g1>>g2;
    while(pa<=pb && ans<=100){
      ans++;
      pa += floor((pa * g1)/(double)100);
      pb += floor((pb * g2)/(double)100);
    }
    if(ans > 100)
      cout<<"Mais de 1 seculo."<<endl;
    else
      cout<<ans<<" anos."<<endl;
  }
  return 0;
}
