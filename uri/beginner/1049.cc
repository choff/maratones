#include<bits/stdc++.h>

using namespace std;


bool comp(double i, double j){return (i>j);}

int main(){
  unordered_map<string, unordered_map<string, unordered_map<string,string> > > M;
  M["vertebrado"]["ave"]["carnivoro"] = "aguia";
  M["vertebrado"]["ave"]["onivoro"] = "pomba";
  M["vertebrado"]["mamifero"]["onivoro"] = "homem";
  M["vertebrado"]["mamifero"]["herbivoro"] = "vaca";
  M["invertebrado"]["inseto"]["hematofago"] = "pulga";
  M["invertebrado"]["inseto"]["herbivoro"] = "lagarta";
  M["invertebrado"]["anelideo"]["hematofago"] = "sanguessuga";
  M["invertebrado"]["anelideo"]["onivoro"] = "minhoca";

  string a,b,c;
  cin>>a>>b>>c;
  cout<<M[a][b][c]<<endl;
  return 0;
}
