#include<bits/stdc++.h>

using namespace std;

int main(){
  double A,B,C,D,flag;
  cin>>A>>B>>C;

  D = B*B - (4*A*C);
  if(A!=0 && D >= 0) {
    D = sqrt(D);
    cout<<fixed<<setprecision(5)<<"R1 = "<< (-B + D)/(2*A)<<endl;
    cout<<fixed<<setprecision(5)<<"R2 = "<< (-B - D)/(2*A)<<endl;
  }
  else {
    cout<<"Impossivel calcular"<<endl;
  }
  return 0;
}
