#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,i;
  while(cin>>n) {
    vector<vector<int> > arr(n, vector<int>(n,3));
    i = 0;
    for(int x=0; x<n/2; ++x,i++) {
      arr[x][i] = 1;
      arr[n-1-x][n-1-i] = 1;
      arr[x][n-1-i] = 2;
      arr[n-1-x][i] = 2;
    }
    if(n%2!=0)
      arr[n/2][n/2] = 2;

    for(int a=0; a<n; a++){
      for(int b=0; b<n; b++)
        cout<<arr[a][b];
      cout<<endl;
    }
  }
  return 0;
}
