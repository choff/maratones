#include<bits/stdc++.h>

using namespace std;

int main() {
  int n,tmp;
  while(cin>>n && n){
    int arr[n][n];
    for(int i=0; i< n; i++) {
      tmp=i+2;
      for(int j=0; j< n; ++j) {
        if(j>i) tmp++;
        else if(j<i) tmp--;
        else if(j==i) tmp=1;
        arr[i][j] = tmp;
      }
    }
    for(int i=0; i<n; ++i){
      for(int j=0; j<n; ++j){
        printf("%3d",arr[i][j]);
        if(j != n-1) printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
  return 0;
}
