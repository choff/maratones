#include<bits/stdc++.h>

#define MAX 100

using namespace std;

int main() {
  int min=1<<30, pos=0,tmp,n,i=0;
  cin>>n;
  while(n--) {
    cin>>tmp;
    if(tmp<min) {
      min = tmp;
      pos = i;
    }
    i++;
  }
  cout<<"Menor valor: "<<min<<endl;
  cout<<"Posicao: "<<pos<<endl;
  return 0;
}
