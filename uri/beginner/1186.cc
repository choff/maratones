#include<bits/stdc++.h>

#define MAX 100

using namespace std;

int main() {
  double M[12][12];
  char op;
  double ans=0;
  cin>>op;
  for(int i=0; i<12; ++i)
    for(int j=0; j<12; ++j)
      cin>>M[i][j];

  for(int k=1; k<12; ++k)
    for(int l=12-k; l<12; ++l)
      ans += M[k][l];

  if(op == 'M')
    ans = ans/66;
  cout<<fixed<<setprecision(1)<<ans<<endl;
  return 0;
}
