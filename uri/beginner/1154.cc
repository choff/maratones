#include<bits/stdc++.h>

using namespace std;

int main() {
  int tmp,n=0,ans=0;
  while(cin>>tmp && tmp>0){
    n++;
    ans+=tmp;
  }
  cout<<fixed<<setprecision(2)<<ans/(double)n<<endl;
  return 0;
}
