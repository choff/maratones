#include<bits/stdc++.h>

#define MAX 100

using namespace std;

int main() {
  double M[12][12];
  int col;
  char op;
  double ans=0;
  cin>>col>>op;
  for(int i=0; i<12; ++i)
    for(int j=0; j<12; ++j)
      cin>>M[i][j];
  for(int k=0; k<12; ++k)
    ans += M[k][col];
  if(op == 'M')
    ans = ans/12;
  cout<<fixed<<setprecision(1)<<ans<<endl;
  return 0;
}


/*
par  :4,-4,2,8,2
impar:1,3,3,5,-7XW


 */
