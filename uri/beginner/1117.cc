#include<bits/stdc++.h>

using namespace std;

int main() {
  int valid=0;
  double tmp,ans=0;
  while(valid<2) {
    cin>>tmp;
    if(tmp<0 || tmp>10)
      cout<<"nota invalida"<<endl;
    else {
      ans+=tmp;
      valid++;
    }
  }
  cout<<fixed<<setprecision(2)<<"media = "<<ans/(double)2<<endl;
  return 0;
}
