#include<bits/stdc++.h>

using namespace std;

int main(){
  int n,min=1<<30,i=1,tmp,ans;
  cin>>n;
  while(n--){
    cin>>tmp;
    if(tmp<min){
      ans=i;
      min=tmp;
    }
    i++;
  }
  cout<<ans<<endl;
  return 0;
}
