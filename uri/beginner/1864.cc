//Life is not a problem to be solved
#include<bits/stdc++.h>

using namespace std;

int main() {
  char arr[]={'L','I','F','E',' ','I','S',' ','N','O','T',' ','A',' ','P','R','O','B','L','E','M',' ','T','O',' ','B','E',' ','S','O','L','V','E','D'};
  int n;
  while(cin>>n) {
    for(int i=0; i<n; ++i)cout<<arr[i];
    cout<<endl;
  }
  return 0;
}
