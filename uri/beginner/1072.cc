#include<bits/stdc++.h>

using namespace std;

int main() {
  int in=0,out=0,n,tmp;
  cin>>n;
  while(n--) {
    cin>>tmp;
    (tmp>=10 && tmp<=20)?  in++ : out++;
  }
  cout<<in<<" in"<<endl;
  cout<<out<<" out"<<endl;
  return 0;
}
