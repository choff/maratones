#include<bits/stdc++.h>

using namespace std;

int main() {
  int x,y,tmp;
  while(cin>>x>>y && x>0 && y>0) {
    tmp = x;
    x = min(x,y);
    y = max(tmp,y);
    for(int i=x; i<=y; ++i) cout<<i<<" ";
    int sum = (y*(y+1)/2) - (x*(x-1)/2);
    cout<<"Sum="<<sum<<endl;
  }
  return 0;
}
