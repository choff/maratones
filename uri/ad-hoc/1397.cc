#include<bits/stdc++.h>

using namespace std;

int main() {
  int N,a,b;
  while(cin>>N && N) {
    int A=0,B=0;
    for(int i=0; i<N; ++i) {
      cin>>a>>b;
      if(a>b) A++;
      else if(a<b) B++;
    }
    cout<<A<<" "<<B<<endl;
  }
  return 0;
}
