#include<bits/stdc++.h>

using namespace std;

int main(){
    int n;
    cin>>n;
    while(n!=0){
        int flag=1, m=0;
        list<int> L;
        for(int i=0; i<n; ++i)
            L.push_back(i+1);
        while(flag) {
            m++;
            list<int> C = L;
            int cont=1, k=0;
            while(true) {
                auto it = C.begin();
                advance(it,k);
                if(*it == 13) {
                    break;
                }
                if(cont == n-1) {
                    flag = 0;
                    break;
                }
                C.erase(it);
                cont++;
                k += m -1 ;
                k = k % C.size();
            };
        }
        cout<<m<<endl;
        cin>>n;
    }
    return 0;
}
