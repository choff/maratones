#include<bits/stdc++.h>

using namespace std;

int main(){
  int n,m;
  while(cin>>n>>m && m && n) {
    int mat[n][m],f1=1,f4=1,ans=0;
    vector<int> f2(m,0); //check if some 0 exist
    vector<int> f3(m,1); //check if some 1 exist
    for(int i=0; i<n; ++i) {
      int tmp=0;
      for(int j=0; j<m; ++j) {
        cin>>mat[i][j];
        if(mat[i][j] == 1) {
          tmp++;
          f2[j] = 1;
        }
        else
          f3[j] = 0;
      }
      if(tmp == 0) f4=0;
      if(tmp == m) f1=0;
    }
    ans = f1 + f4;
    int f=1;
    for(int i=0; i<m; ++i)
      if(f2[i] == 0)
        f=0;
    ans += f;
    f=1;
    for(int i=0; i<m; ++i)
      if(f3[i] == 1)
        f=0;
    ans += f;
    cout<<ans<<endl;
  }
  return 0;
}
