#include<bits/stdc++.h>

using namespace std;

int solve(int N, int M, int p[], int w, int len) {
  int l=0, r=len-1, f=1, ans=0;
  N = N*100;
  if(N % w != 0)
    return 1<<30;

  while(f) {
    if (N==0  || r == l) break;
    int tmp = p[r] + p[l];
    if(p[r] == M) {
      ans++, r--, N -= w;
    }
    else if(tmp == M) {
      ans+=2, r--, l++, N -= w;
    }
    else if(tmp > M) r--;
    else if(tmp < M) l++;
  }
  return ((N==0)? ans : 1<<30);
};

int main(){
  int N,M,width,planks;
  cin>>N>>M;
  while (N!=0 && M!=0) {
    cin>>width>>planks;
    int plank[planks];
    for(int i=0; i<planks; ++i) cin>>plank[i];
    sort(plank,plank + planks);

    int ans  = min(solve(N,M,plank,width,planks),
                   solve(M,N,plank,width,planks));
    if(ans == 1<<30)
      cout<<"impossivel"<<endl;
    else
      cout<<ans<<endl;
    cin>>N>>M;
  }
  return 0;
}
