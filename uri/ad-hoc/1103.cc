#include<bits/stdc++.h>

using namespace std;

int main(){
  int h1,h2,m1,m2,f=1;
  cin>>h1>>m1>>h2>>m2;
  if(h1==0 && h2==0 && m1==0 && m2==0)f=0;
  while(f) {
    int ans=0;
    if(h1 <= h2) ans = (h2 - h1) * 60;
    else ans = (24 - h1 + h2) * 60;

    if( m1 <= m2 ) ans += m2-m1;
    else ans += 60 - 60 - m1 + m2;

    if(h1 == h2 && m1 > m2)
      ans = 23*60 + (60 - m1 + m2);

    cout<<ans<<endl;
    cin>>h1>>m1>>h2>>m2;
    if(h1==0 && h2==0 && m1==0 && m2==0)f=0;
  }
  return 0;
}
