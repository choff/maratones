#include<bits/stdc++.h>

using namespace std;

int main(){
  int N,size;
  char c;
  // hash  size -> [numberOfD, numberOfE ]
  while(cin>>N && N) {
    int ans=0;
    unordered_map<int, vector<int> > M;
    for(int i=0; i<N; ++i){
      cin>>size>>c;
      if(!M.count(size)) {
        M[size].push_back(0);
        M[size].push_back(0);
      }
      (c=='D')? M[size][0]++ : M[size][1]++;
    }
    for(auto& item: M) {
      vector<int> x = item.second;
      ans += min(x[0],x[1]);
    }
    cout<<ans<<endl;
  }
  return 0;
}
