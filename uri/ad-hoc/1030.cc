// this is not my solution, after few TLE and runtime error
// i decide look for solution and i was on other way... son thanks to http://cupc71.blogspot.com.co/2015/01/uri-oj-flavious-josephus-legend-1030.html for solution.

#include<stdio.h>
int josephus(int n,int k);
int main()
{
  int tcase,n,k,i;
  while(scanf("%d",&tcase)==1){
    for(i=1;i<=tcase;i++){
      scanf("%d %d",&n,&k);
      int tmp=josephus(n,k);
      printf("n %d  l %d\n",n,k);
      printf("Case %d: %d\n",i,tmp);
    }
  }
  return 0;
}
int josephus(int n,int k)
{
  printf("--- n %d  l %d\n",n,k);
  if(n==1)
    return 1;
  else
    return (josephus(n-1,k)+k-1)%n+1;
}
