#include<bits/stdc++.h>

using namespace std;

int main(){
  int N,f, arr[5];
  char c[5] = {'A','B','C','D','E'};
  while(cin>>N && N) {
    for(int k = 0; k < N; ++k) {
      f=0;
      char ans;
      for(int i=0; i<5; ++i) {
        cin>>arr[i];
        if(arr[i] <= 127)  {
          f++, ans = c[i];
        }
      }
      cout<<((f==1)? ans:'*')<<endl;
    }
  }
  return 0;
}
