#include<bits/stdc++.h>

using namespace std;

const int MAX=50000;

int arr[MAX];
int prime[MAX];

int N;

int J(int n, int i){
  if(n == 1)
    return 0;
  int k = prime[i++];
  return (J(n-1,i) + k) % n;
}

int main(){
  int p = 0;
  memset(arr,1,sizeof(int)*MAX);
  for(int i=2; i<MAX; ++i) {
    if(arr[i]) {
      prime[p++] = i;
      for(int j=2; j*i < MAX; ++j)
        arr[j*i]= 0;
    }
  }

  while(cin>>N && N) {
    cout<<J(N,0)+1<<endl;
  }

  return 0;
}
