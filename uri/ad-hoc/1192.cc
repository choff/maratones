#include<bits/stdc++.h>
using namespace std;

int main(){
    int N,a,b;
    string cad;
    cin>>N;
    while(N--){
        cin>>cad;
        a = cad[0] - '0', b = cad[2] - '0';
        if(a == b) cout<< a*b <<endl;
        else if(isupper(cad[1])) cout<<b-a<<endl;
        else cout<<a+b<<endl;
    }
    return 0;
}
