#include<bits/stdc++.h>
using namespace std;

int main(){
    int N;
    char pos[] = {'N','L','S','O'};
    string cad;
    while(cin>>N && N){
        cin>>cad;
        int k=0;
        for(int i=0; i<N; ++i){
            (cad[i] == 'D')? k++ : k--;
            if(k<0)k=3;
            else if(k>3)k=0;
        }
        cout<<pos[k]<<endl;
    }
    return 0;
}
