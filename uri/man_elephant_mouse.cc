#include<bits/stdc++.h>

#define vn vector<Node>

using namespace std;

struct Node{
  int H;
  int E;
  int R;
};

//sobrecarga de operador
Node operator +(const Node &a, const Node &b){
  Node c = {a.H + b.H , a.E + b.E, a.R + b.R};
  return c;
}

//swap H->E->R
void swapNode(Node &x){
  int tmp = x.H;
  x.H = x.R;
  x.R = x.E;
  x.E = tmp;
}

int arr[400000];
int Lazy[400000];

int getMid(int s, int e){return s + (e-s)/2;}
void load(int arr[], int N){ for(int i=0; i<N; i++)arr[i] = 0;}

void printArr(int arr[], int N){
  for(int i=0; i<N; i++)
    cout<<arr[i]<<" ";
  cout<<endl;
}

void printST(vn &ST, int N){
  cout<<endl<<"ST:"<<endl;
  for(int i=1; i<N; ++i) {
    cout<<i<<"-> "<<ST[i].H<<" "<<ST[i].E<<" "<<ST[i].R<<endl;
  }
}
//by problem definition i dont need original array
Node buildST(int ss, int se, int node, vn &ST){
  if(ss == se){
    struct Node hoja = {1,0,0};
    return ST[node] = hoja;
  }

  int mid  = getMid(ss,se);

  return ST[node] = buildST(ss, mid, 2*node, ST) +
    buildST(mid+1, se, 2*node+1, ST);
}

void updateST(int ss, int se, int node, int l, int r, vn &ST){
  if(Lazy[node]){
    for(int i=0; i < Lazy[node]; ++i) {
      swapNode(ST[node]);
      //has children
      if(ss != se){
        Lazy[2*node] = (Lazy[2*node] + 1 ) % 3;
        Lazy[2*node+1] = (Lazy[2*node+1] +1 ) % 3;
      }
    }
    Lazy[node] = 0;
  }

  if(ss > r || se < l) return;

  if(ss >= l && se <= r){
    swapNode(ST[node]);
    if(ss != se) {
      Lazy[2*node] = (Lazy[2*node] + 1 ) % 3;
      Lazy[2*node+1] = (Lazy[2*node+1] +1 ) % 3;
    }
    return;
  }

  int mid = getMid(ss,se);
  updateST(ss, mid, 2*node, l, r, ST);
  updateST(mid+1, se, 2*node+1, l, r, ST);
  ST[node] = ST[2*node] + ST[2*node+1];

}

Node queryRangeST(int ss, int se, int node, int l, int r, vn &ST){
  if(Lazy[node]){
    for(int i=0; i < Lazy[node]; ++i) {
      swapNode(ST[node]);
      //has children
      if(ss != se){
        Lazy[2*node] = (Lazy[2*node] + 1 ) % 3;
        Lazy[2*node+1] = (Lazy[2*node+1] +1 ) % 3;
      }
    }
    Lazy[node] = 0;
  }
  //out of range
  if(ss > r || se < l) {
    Node empty = {0,0,0};
    return empty;
  }
  //inside range
  if(ss >= l && se <= r){
    return ST[node];
  }

  int mid = getMid(ss,se);
  return queryRangeST(ss, mid, 2*node, l, r, ST) +
    queryRangeST(mid+1, se, 2*node+1, l, r, ST);
}

int main(){
  int N,M;
  while(cin>>N>>M){
    int A,B;
    char cmd;

    int height = ceil(log2(N));
    int len = 4 * pow(2,height);

    vn ST(len);

    memset(arr,0,sizeof(int)*400000);
    memset(Lazy,0,sizeof(int)*400000);

    buildST(0, N-1, 1, ST);

    while(M--){
      cin>>cmd>>A>>B;
      if(cmd == 'M')
        updateST(0, N-1, 1, A-1, B-1, ST);
      else{
        Node ans = queryRangeST(0, N-1, 1, A-1, B-1, ST);
        cout<<ans.H<<" "<<ans.E<<" "<<ans.R<<endl;
      }
    }
    cout<<endl;
  }
  return 0;
}
